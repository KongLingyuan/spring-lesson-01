package com.konglingyuan.spring01.run;

import com.konglingyuan.spring01.dao.Pet;

public class Person {
	
	private Pet pet;
	
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	
	public void feedPet(String string) {
		pet.eat(string);
	}
}
