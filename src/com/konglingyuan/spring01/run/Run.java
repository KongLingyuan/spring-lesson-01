package com.konglingyuan.spring01.run;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Run {

	public static void main(String[] args) {
//		Dog dog = new Dog();
//		dog.eat("骨头");
//		
//		Person person = new Person();
//		person.feedPet(dog);
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("./application.xml");
		Person person = (Person)ac.getBean("person");
		person.feedPet("骨头");
	}

}
