package com.konglingyuan.spring01.dao.impl;

import com.konglingyuan.spring01.dao.Pet;

public class Cat implements Pet {

	@Override
	public void eat(String food) {
		System.out.println("小猫吃" + food);
	}

}
