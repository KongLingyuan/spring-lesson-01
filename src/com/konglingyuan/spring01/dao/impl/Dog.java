package com.konglingyuan.spring01.dao.impl;

import com.konglingyuan.spring01.dao.Pet;

public class Dog implements Pet {
	private String name;
	
	public String getName() {
		return name;
	}

	@Override
	public void eat(String food) {
		System.out.println("小狗吃" + food);
	}

	@Override
	public String toString() {
		return "Dog";
	}
	
	

}
